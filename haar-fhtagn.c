#include <stdio.h>					//	в argv[1] передавать имя папки, где лежат образцы, а он извлечет из них средние размеры рамки и среднее ее положение
#include <stdlib.h>
#include <string.h>
#include <dirent.h>

#include </home/ermak2/prog/bmp_functions.c>

main (int argc, char* argv[]) {
	
	FILE *file;					//	Это файл. В него мы будем записывать результаты. Мне лень возиться с удобочитаемостью файла для людей, так что хватит с него просто чисел в определенной последовательности.
								//	Формат файла - числа, идущие через enter. Всего чисел 26. Первые 18 - целые (int), остальные 8 - float. Последовательность чисел: первые 2 - высота образца и его ширина, следующие 8 - размеры признаков, следующие 8 - их координаты, и еще 8 - средние коэффициенты похожести (разность, деленная на размер признака) и СКО каждого.
								//	-1 - sampleheight, 0 - samplewidth;
								//	1 - aheight, 2 - awidth, 3 - bheight, 4 - bwidth, 5 - cheight, 6 - cwidth, 7 - dheight, 8 - dwidth;
	DIR *directory;				//	9 - xa, 10 - ya, ...   ---//---
	struct dirent *dir;				//	17 - agva, 18 - sigma1, ...
	
	struct myimage im;
	im.debug = 0;
	
	char *filename, *str;
	long long diff, s1, s2, s3, s4, maxdiff, mindiff, maxindex, absmax, sum, max;
	long long **image, **sample, **map;
	int i, j, x, y, k, l, count, t,
		aheight, awidth,			//	среднее значение высоты и ширины для каждого признака
		bheight, bwidth,
		cheight, cwidth,
		dheight, dwidth,
		ax, ay, bx, by, cx, cy, dx, dy;
	int **sizea, **coorda,			//	в эти массивы будем складывать наилучшую высоту и наилучшие координаты для каждой фотки, чтобы потом их усреднить
		**sizeb, **coordb,
		**sizec, **coordc,
		**sized, **coordd;
	float avg, avga, avgb, avgc, avgd, sigma1, sigma2, sigma3, sigma4, **difference;
		
	int height, width;
	
	
	
	if ( (directory = opendir(argv[1])) == NULL ) {
		perror("Open directory");
		exit(EXIT_SUCCESS);
	}
	
	if (argv[1][strlen(argv[1]) - 1] != '/')
		strcat(argv[1], "/");
	
	count = 0;
	while ( (dir = readdir(directory) ) != NULL ) {
		if (dir->d_type == 8)
			count++;
	}
	
	rewinddir(directory);
	
	sizea = (int**) malloc (sizeof(int*)*count);			//	массив, в который будем складывать размеры, чтобы их потом усреднить по всем файлам
	coorda = (int**) malloc (sizeof(int*)*count);			//	массив, в который будем складывать координаты признака оптимального размера, чтобы их потом усреднить по всем файлам
	sizeb = (int**) malloc (sizeof(int*)*count);
	coordb = (int**) malloc (sizeof(int*)*count);
	sizec = (int**) malloc (sizeof(int*)*count);
	coordc = (int**) malloc (sizeof(int*)*count);
	sized = (int**) malloc (sizeof(int*)*count);
	coordd = (int**) malloc (sizeof(int*)*count);
	difference = (float**) malloc(sizeof(float*)*count);		//	Да будет здесь храниться разница каждого признака с найденным размером и найденным местом, деленная на размер этого признака, дабы после усреднить ее и вычислить СКО
	for (i = 0; i < count; i++) {
		sizea[i] = (int*) malloc (sizeof(int)*2);		//	Постановляю в первый столбец записывать высоту признака, во второй - его ширину
		coorda[i] = (int*) malloc (sizeof(int)*2);		//	Впредь в первый столбец заносить координату x (i), во второй - координату y (j). Да будет так.
		sizeb[i] = (int*) malloc (sizeof(int)*2);
		coordb[i] = (int*) malloc (sizeof(int)*2);
		sizec[i] = (int*) malloc (sizeof(int)*2);
		coordc[i] = (int*) malloc (sizeof(int)*2);
		sized[i] = (int*) malloc (sizeof(int)*2);
		coordd[i] = (int*) malloc (sizeof(int)*2);
		difference[i] = (float*) malloc(sizeof(float)*4);		//	Повелеваю в первый столбец с номером 0 запоминать разницы признака А, во второй (1) - признака B и так далее
	}

	filename = (char*) malloc(sizeof(char)*(strlen(argv[1]) + 256));

	do {
		dir = readdir(directory);
	}
	while (dir->d_type != 8);
	
	strcpy(filename, argv[1]);
	strcat(filename, dir->d_name);
	getinfbmp(filename, &im);
	
	rewinddir(directory);
	
	im.gr = (unsigned char**) malloc(sizeof(unsigned char*)*im.height);
	image = (long long**) malloc (sizeof(long long*)*im.height);
	for (i = 0; i < im.height; i++) {
		im.gr[i] = (unsigned char*) malloc(sizeof(unsigned char)*im.width);
		image[i] = (long long*) malloc (sizeof(long long)*im.width);
	}

	t = 0;

	while ( (dir = readdir(directory) ) != NULL ) {
		
		if (dir->d_type != 8)			//	понятно
			continue;
			
		strcpy(filename, argv[1]);
		strcat(filename, dir->d_name);
		openbmp(filename, &im);			//	открыли
		
		printf("#%i %s\n", t, dir->d_name);
		
		for (i = 0; i < im.height; i++)
			for (j = 0; j < im.width; j++) {
				im.gr[i][j] = gray(im.r[i][j], im.g[i][j], im.b[i][j]);
				image[i][j] = im.gr[i][j];
			}								//	очернобелили
			
		for (i = 1; i < im.height; i++)
			image[i][0] += image[i-1][0];
		for (j = 1; j < im.width; j++)
			image[0][j] += image[0][j-1];
		for (i = 1; i < im.height; i++)
			for (j = 1; j < im.width; j++)
				image[i][j] = image[i][j] - image[i-1][j-1] + image[i-1][j] + image[i][j-1];		//	представили
//--------------------------------	A
		maxindex = 0;
		absmax = 0;
		for (height = 1; height < im.height; height++)
			for (width = 1; width < im.width/2; width++) {
				maxdiff = 0;
				for (i = 1; i < im.height - height; i++)
					for (j = 1; j < im.width - 2*width; j++) {
						s1 = image[i+height][j+width] + image[i][j] - image[i+height][j] - image[i][j+width];	//	clear
						s2 = image[i+height][j+width + width] + image[i][j + width] - image[i+height][j + width] - image[i][j+width + width];	//	shaded
						diff = s2-s1;
	
						if (diff > maxdiff)
							maxdiff = diff;
					}
	
				if (1.*maxdiff/(height*width) > absmax)
					absmax = 1.*maxdiff/(height*width);
				if (1.*maxdiff/(height*width) > maxindex && height > im.height/2 && width > im.width/20) {
					maxindex = 1.*maxdiff/(height*width);
					x = height;
					y = width;
				}
			}
		printf(" A: optimal height = %i, width = %i\n", x, y);
		sizea[t][0] = x;
		sizea[t][1] = y;
//--------------------------------	B
		maxindex = 0;
		absmax = 0;
		for (height = 1; height < im.height/2; height++)
			for (width = 1; width < im.width; width++) {
	
				maxdiff = 0;
				for (i = 1; i < im.height - 2*height; i++)
					for (j = 1; j < im.width - width; j++) {
						s1 = image[i+height][j+width] + image[i][j] - image[i+height][j] - image[i][j+width];		//	shaded
						s2 = image[i+height + height][j+width] + image[i + height][j] - image[i+height + height][j] - image[i + height][j+width];		//	clear
						diff = s1-s2;
	
						if (diff > maxdiff)
							maxdiff = diff;
					}
	
				if (1.*maxdiff/(height*width) > absmax)
					absmax = 1.*maxdiff/(height*width);
				if (1.*maxdiff/(height*width) > maxindex && height > im.height/10 && width > im.width/2) {
					maxindex = 1.*maxdiff/(height*width);
					x = height;
					y = width;
				}
			}
		printf(" B: optimal height = %i, width = %i\n", x, y);
		sizeb[t][0] = x;
		sizeb[t][1] = y;
//--------------------------------	C
		maxindex = -1000000000;
		absmax = -1000000000;
		for (height = 1; height < im.height; height++)
			for (width = 1; width < im.width; width++) {
				maxdiff = -100000000;
				for (i = 0; i < im.height - height; i++)
					for (j = 0; j < im.width - 3*width; j++) {
						s1 = image[i+height][j+width] + image[i][j] - image[i+height][j] - image[i][j+width];	//	clear
						s2 = image[i+height][j+width + width] + image[i][j + width] - image[i+height][j + width] - image[i][j+width + width];	//	shaded
						s3 = image[i+height][j+width + width + width] + image[i][j + width + width] - image[i+height][j + width + width] - image[i][j+width + width + width];	//	clear
						diff = s2 - s1 - s3;
						
						diff = -diff;			//	тогда получается положительное число, потому что сумма крайних прямоугольников все равно больше среднего прямоугольника
	
						if (diff > maxdiff)
							maxdiff = diff;
					}
	
				if (1.*maxdiff/(height*width) > absmax)
					absmax = 1.*maxdiff/(height*width);
				if (1.*maxdiff/(height*width) > maxindex && height > im.height/2 && width > im.width/10) {
					maxindex = 1.*maxdiff/(height*width);
					x = height;
					y = width;
				}
			}
		printf(" С: optimal height = %i, width = %i\n", x, y);
		sizec[t][0] = x;
		sizec[t][1] = y;
//--------------------------------	D
		maxindex = -1000000000;
		absmax = -1000000000;
		for (height = 1; height < im.height/2; height++)
			for (width = 1; width < im.width/2; width++) {
				maxdiff = -100000000;
				mindiff = 100000000;
				for (i = 0; i < im.height - 2*height; i++)
					for (j = 0; j < im.width - 2*width; j++) {
						s1 = image[i+height][j+width] + image[i][j] - image[i+height][j] - image[i][j+width];	//	clear
						s2 = image[i+height][j+width + width] + image[i][j + width] - image[i+height][j + width] - image[i][j+width + width];	//	shaded
						s3 = image[i+height + height][j+width] + image[i + height][j] - image[i+height + height][j] - image[i + height][j+width];
						s4 = image[i+height + height][j+width + width] + image[i + height][j + width] - image[i+height + height][j + width] - image[i + height][j+width + width];
						diff = s1 + s4 - s2 - s3;
						
						if (diff > maxdiff)
							maxdiff = diff;
					}
	
				if (1.*maxdiff/(height*width) > absmax)
					absmax = 1.*maxdiff/(height*width);
				if (1.*maxdiff/(height*width) > maxindex) {
					maxindex = 1.*maxdiff/(height*width);
					x = height;
					y = width;
				}
			}
		printf(" D: optimal height = %i, width = %i\n", x, y);
		sized[t][0] = x;
		sized[t][1] = y;
//--------------------------------
		t++;
	}
//---------------------------------------------------------------------------	нахождение среднего положения:	
	s1 = 0;
	s2 = 0;
	for (i = 0; i < count; i++) {
		s1 += sizea[i][0];
		s2 += sizea[i][1];
	}
	
	avg = 1.*s1/count;
	aheight = avg + .5;
	printf("\nA: average height = %f\n", avg);
	avg = 1.*s2/count;
	awidth = avg + .5;
	printf("\nA: average width = %f\n", avg);
	
	s1 = 0;
	s2 = 0;
	for (i = 0; i < count; i++) {
		s1 += sizeb[i][0];
		s2 += sizeb[i][1];
	}
	
	avg = 1.*s1/count;
	bheight = avg + .5;
	printf("\nB: average height = %f\n", avg);
	avg = 1.*s2/count;
	bwidth = avg + .5;
	printf("\nB: average width = %f\n", avg);
	
	s1 = 0;
	s2 = 0;
	for (i = 0; i < count; i++) {
		s1 += sizec[i][0];
		s2 += sizec[i][1];
	}
	
	avg = 1.*s1/count;
	cheight = avg + .5;
	printf("\nC: average height = %f\n", avg);
	avg = 1.*s2/count;
	cwidth = avg + .5;
	printf("\nC: average width = %f\n", avg);
	
	s1 = 0;
	s2 = 0;
	for (i = 0; i < count; i++) {
		s1 += sized[i][0];
		s2 += sized[i][1];
	}
	
	avg = 1.*s1/count;
	dheight = avg + .5;
	printf("\nD: average height = %f\n", avg);
	avg = 1.*s2/count;
	dwidth = avg + .5;
	printf("\nD: average width = %f\n", avg);
	
//--------------------------------------------------------------------------------------	Проходимся по всем картинкам, узнаем, на каких местах лучше поставить каждый признак

	if ( (file = fopen("data.txt", "wb")) == NULL) {
		perror("Data writing");
		exit(EXIT_SUCCESS);
	}

	//				Если долго считать неохота, то вот результаты предыдущего while'а на выборке из 73-х фоток моего фейса.
//	aheight = 71;
//	awidth = 16;
//	bheight = 27;
//	bwidth = 97;
//	cheight = 72;
//	cwidth = 25;
//	dheight = 28;
//	dwidth = 19;


	sprintf(str, "%i\n", im.height);
	fputs(str, file);
	sprintf(str, "%i\n", im.width);
	fputs(str, file);

	sprintf(str, "%i\n", aheight);
	fputs(str, file);
	sprintf(str, "%i\n", awidth);
	fputs(str, file);
	sprintf(str, "%i\n", bheight);
	fputs(str, file);
	sprintf(str, "%i\n", bwidth);
	fputs(str, file);
	sprintf(str, "%i\n", cheight);
	fputs(str, file);
	sprintf(str, "%i\n", cwidth);
	fputs(str, file);
	sprintf(str, "%i\n", dheight);
	fputs(str, file);
	sprintf(str, "%i\n", dwidth);
	fputs(str, file);

//------------------------------------------------------------------	Находим наилучшее место, куда приложить шаблон с найденными размерами
	
	rewinddir(directory);
	
	t = 0;
	
max = 0;

	while ( (dir = readdir(directory)) != NULL) {
	
		if (dir->d_type != 8)			//	понятно
			continue;
			
		strcpy(filename, argv[1]);
		strcat(filename, dir->d_name);
		openbmp(filename, &im);			//	открыли
		
		for (i = 0; i < im.height; i++)
			for (j = 0; j < im.width; j++) {
				im.gr[i][j] = gray(im.r[i][j], im.g[i][j], im.b[i][j]);
				image[i][j] = im.gr[i][j];
			}								//	очернобелили
			
		for (i = 1; i < im.height; i++)
			image[i][0] += image[i-1][0];
		for (j = 1; j < im.width; j++)
			image[0][j] += image[0][j-1];
		for (i = 1; i < im.height; i++)
			for (j = 1; j < im.width; j++)
				image[i][j] = image[i][j] - image[i-1][j-1] + image[i-1][j] + image[i][j-1];		//	представили

//---------------------------- A

		x = -1;
		y = -1;
				
		maxdiff = 0;
		for (i = 0; i < im.height - aheight; i++)
			for (j = 0; j < im.width - 2*awidth; j++) {
				s1 = image[i+aheight][j+awidth] + image[i][j] - image[i+aheight][j] - image[i][j+awidth];
				s2 = image[i+aheight][j+awidth + awidth] + image[i][j + awidth] - image[i+aheight][j + awidth] - image[i][j+awidth + awidth];
				
				diff = s2 - s1;
				if (diff > maxdiff) {
					maxdiff = diff;
					x = i;
					y = j;
				}
			}
if (maxdiff > max) max = 1.*maxdiff/(aheight*awidth);
		printf("A: optimal place: x = %i, y = %i\n", x, y);
		coorda[t][0] = x;
		coorda[t][1] = y;
//---------------------------- B	
	
		x = -1;
		y = -1;
		
		maxdiff = 0;
		for (i = 0; i < im.height - 2*bheight; i++)
			for (j = 0; j < im.width - bwidth; j++) {
				s1 = image[i+bheight][j+bwidth] + image[i][j] - image[i+bheight][j] - image[i][j+bwidth];
				s2 = image[i+bheight + bheight][j+bwidth] + image[i + bheight][j] - image[i+bheight + bheight][j] - image[i + bheight][j+bwidth];
				
				diff = s1 - s2;
	
				if (diff > maxdiff) {
					maxdiff = diff;
					x = i;
					y = j;
				}
			}
if (maxdiff > max) max = 1.*maxdiff/(bheight*bwidth);
		printf("B: optimal place: x = %i, y = %i\n", x, y);
		coordb[t][0] = x;
		coordb[t][1] = y;

//---------------------------	C

		x = -1;
		y = -1;
	
		maxdiff = -100000000;
		for (i = 0; i < im.height - cheight; i++)
			for (j = 0; j < im.width - 3*cwidth; j++) {
				s1 = image[i+cheight][j+cwidth] + image[i][j] - image[i+cheight][j] - image[i][j+cwidth];	//	ccwidthear
				s2 = image[i+cheight][j+cwidth + cwidth] + image[i][j + cwidth] - image[i+cheight][j + cwidth] - image[i][j+cwidth + cwidth];	//	shaded
				s3 = image[i+cheight][j+cwidth + cwidth + cwidth] + image[i][j + cwidth + cwidth] - image[i+cheight][j + cwidth + cwidth] - image[i][j+cwidth + cwidth + cwidth];	//	ccwidthear
				diff = s2 - s1 - s3;
//				diff = -diff;
				if (diff > maxdiff) {
					maxdiff = diff;
					x = i;
					y = j;
				}
			}
if (maxdiff > max) max = 1.*maxdiff/(cheight*cwidth);
		printf("C: optimal place: x = %i, y = %i\n", x, y);
		coordc[t][0] = x;
		coordc[t][1] = y;
//---------------------------	D

		x = -1;
		y = -1;
		
		maxdiff = -100000000;
		for (i = 0; i < im.height - 2*dheight; i++)
			for (j = 0; j < im.width - 2*dwidth; j++) {
				s1 = image[i+dheight][j+dwidth] + image[i][j] - image[i+dheight][j] - image[i][j+dwidth];	//	cdwidthear
				s2 = image[i+dheight][j+dwidth + dwidth] + image[i][j + dwidth] - image[i+dheight][j + dwidth] - image[i][j+dwidth + dwidth];	//	shaded
				s3 = image[i+dheight + dheight][j+dwidth] + image[i + dheight][j] - image[i+dheight + dheight][j] - image[i + dheight][j+dwidth];
				s4 = image[i+dheight + dheight][j+dwidth + dwidth] + image[i + dheight][j + dwidth] - image[i+dheight + dheight][j + dwidth] - image[i + dheight][j+dwidth + dwidth];
				diff = s1 + s4 - s2 - s3;
				if (diff > maxdiff) {
					maxdiff = diff;
					x = i;
					y = j;
				}
			}
if (maxdiff > max) max = 1.*maxdiff/(dheight*dwidth);
		printf("D: optimal place: x = %i, y = %i\n", x, y);
		coordd[t][0] = x;
		coordd[t][1] = y;

//--------------------------
		
		t++;
	
	}
printf("\n absolute maximum = %lli\n", max);	
//---------------------------------------------------------------------------------------	считаем среднее положение для каждого признака:

	s1 = 0;
	s2 = 0;
	for (i = 0; i < count; i++) {
		s1 += coorda[i][0];
		s2 += coorda[i][1];
	}
	
	avg = 1.*s1/count;
	ax = avg + .5;
	printf("\nA: average x = %f\n", avg);
	avg = 1.*s2/count;
	ay = avg + .5;
	printf("\nA: average y = %f\n", avg);
	
	s1 = 0;
	s2 = 0;
	for (i = 0; i < count; i++) {
		s1 += coordb[i][0];
		s2 += coordb[i][1];
	}
	
	avg = 1.*s1/count;
	bx = avg + .5;
	printf("\nB: average x = %f\n", avg);
	avg = 1.*s2/count;
	by = avg + .5;
	printf("\nB: average y = %f\n", avg);
	
	s1 = 0;
	s2 = 0;
	for (i = 0; i < count; i++) {
		s1 += coordc[i][0];
		s2 += coordc[i][1];
	}
	
	avg = 1.*s1/count;
	cx = avg + .5;
	printf("\nC: average x = %f\n", avg);
	avg = 1.*s2/count;
	cy = avg + .5;
	printf("\nC: average y = %f\n", avg);
	
	s1 = 0;
	s2 = 0;
	for (i = 0; i < count; i++) {
		s1 += coordd[i][0];
		s2 += coordd[i][1];
	}
	
	avg = 1.*s1/count;
	dx = avg + .5;
	printf("\nD: average x = %f\n", avg);
	avg = 1.*s2/count;
	dy = avg + .5;
	printf("\nD: average y = %f\n", avg);
//-------------------------------	пишем в файл найденные координаты		
	sprintf(str, "%i\n", ax);
	fputs(str, file);
	sprintf(str, "%i\n", ay);
	fputs(str, file);
	sprintf(str, "%i\n", bx);
	fputs(str, file);
	sprintf(str, "%i\n", by);
	fputs(str, file);
	sprintf(str, "%i\n", cx);
	fputs(str, file);
	sprintf(str, "%i\n", cy);
	fputs(str, file);
	sprintf(str, "%i\n", dx);
	fputs(str, file);
	sprintf(str, "%i\n", dy);
	fputs(str, file);
//------------------------------	отрисовываем средние размеры со средним положением
	for (i = ax; i < ax + aheight; i++)
		for (j = ay; j < ay + 2*awidth; j++)
			if (i == ax || i == ax + aheight -1 || j == ay || j == ay + awidth - 1 || j == ay + 2*awidth -1) {
				im.r[i][j] = 255;
				im.g[i][j] = 0;
				im.b[i][j] = 0;
			}
	
	for (i = bx; i < bx + 2*bheight; i++)
		for (j = by; j < by + bwidth; j++)
			if (i == bx || i == bx + bheight -1 || j == by || j == by + bwidth - 1 || i == bx + 2*bheight -1) {
				im.r[i][j] = 0;
				im.g[i][j] = 255;
				im.b[i][j] = 0;
			}
	
	for (i = cx; i < cx + cheight; i++)
		for (j = cy; j < cy + 3*cwidth; j++)
			if (i == cx || i == cx + cheight -1 || j == cy || j == cy + cwidth - 1 || j == cy + 2*cwidth -1 || j == cy + 3*cwidth - 1) {
				im.r[i][j] = 0;
				im.g[i][j] = 0;
				im.b[i][j] = 255;
			}	
	
	for (i = dx; i < dx + 2*dheight; i++)
		for (j = dy; j < dy + 2*dwidth; j++)
			if (i == dx || i == dx + dheight -1 || j == dy || j == dy + dwidth - 1 || j == dy + 2*dwidth -1 || i == dx + 2*dheight - 1) {
				im.r[i][j] = 255;
				im.g[i][j] = 255;
				im.b[i][j] = 0;
			}


	savebmp("haar.bmp", im.height, im.width, im.r, im.g, im.b);			//	сохранили, что нарисовали
//-------------------------------------------------------------------------------------	теперь надо найти, какое наибольшее, наименьшее и среднее значение получается в картинках для каждого признака

	rewinddir(directory);

	t = 0;
	
	while ( (dir = readdir(directory)) != NULL) {
	
		if (dir->d_type != 8)			//	понятно
			continue;
			
		strcpy(filename, argv[1]);
		strcat(filename, dir->d_name);
		openbmp(filename, &im);			//	открыли
		
		for (i = 0; i < im.height; i++)
			for (j = 0; j < im.width; j++) {
				im.gr[i][j] = gray(im.r[i][j], im.g[i][j], im.b[i][j]);
				image[i][j] = im.gr[i][j];
			}								//	очернобелили
			
		for (i = 1; i < im.height; i++)
			image[i][0] += image[i-1][0];
		for (j = 1; j < im.width; j++)
			image[0][j] += image[0][j-1];
		for (i = 1; i < im.height; i++)
			for (j = 1; j < im.width; j++)
				image[i][j] = image[i][j] - image[i-1][j-1] + image[i-1][j] + image[i][j-1];		//	представили	
			
//----------------------	A
		s1 = image[ax+aheight][ay+awidth] + image[ax][ay] - image[ax+aheight][ay] - image[ax][ay+awidth];
		s2 = image[ax+aheight][ay+awidth + awidth] + image[ax][ay + awidth] - image[ax+aheight][ay + awidth] - image[ax][ay+awidth + awidth];
		diff = s2-s1;
		difference[t][0] = 1.*diff/(aheight*awidth);
//----------------------	B
		s1 = image[bx+bheight][by+bwidth] + image[bx][by] - image[bx+bheight][by] - image[bx][by+bwidth];
		s2 = image[bx+bheight + bheight][by+bwidth] + image[bx + bheight][by] - image[bx+bheight + bheight][by] - image[bx + bheight][by+bwidth];
		diff = s1 - s2;
		difference[t][1] = 1.*diff/(bheight*bwidth);
//----------------------	C
		s1 = image[cx+cheight][cy+cwidth] + image[cx][cy] - image[cx+cheight][cy] - image[cx][cy+cwidth];	//	ccwidthear
		s2 = image[cx+cheight][cy+cwidth + cwidth] + image[cx][cy + cwidth] - image[cx+cheight][cy + cwidth] - image[cx][cy+cwidth + cwidth];	//	shaded
		s3 = image[cx+cheight][cy+cwidth + cwidth + cwidth] + image[cx][cy + cwidth + cwidth] - image[cx+cheight][cy + cwidth + cwidth] - image[cx][cy+cwidth + cwidth + cwidth];	//	ccwidthear
		diff = s2 - s1 - s3;
		difference[t][2] = 1.*diff/(cheight*cwidth);
//----------------------	D	
		s1 = image[dx+dheight][dy+dwidth] + image[dx][dy] - image[dx+dheight][dy] - image[dx][dy+dwidth];	//	cdwidthear
		s2 = image[dx+dheight][dy+dwidth + dwidth] + image[dx][dy + dwidth] - image[dx+dheight][dy + dwidth] - image[dx][dy+dwidth + dwidth];	//	shaded
		s3 = image[dx+dheight + dheight][dy+dwidth] + image[dx + dheight][dy] - image[dx+dheight + dheight][dy] - image[dx + dheight][dy+dwidth];
		s4 = image[dx+dheight + dheight][dy+dwidth + dwidth] + image[dx + dheight][dy + dwidth] - image[dx+dheight + dheight][dy + dwidth] - image[dx + dheight][dy+dwidth + dwidth];
		diff = s1 + s4 - s2 - s3;
		difference[t][3] = 1.*diff/(dheight*dwidth);
//---------------------
printf("%s\n", dir->d_name);
printf("A = %f, B = %f, C = %f, D = %f\n", difference[t][0], difference[t][1], difference[t][2], difference[t][3]);
		t++;
	}
//-------------------------------------------------------------------------	
	s1 = 0;
	s2 = 0;
	s3 = 0;
	s4 = 0;
	
	for (i = 0; i < count; i++) {
		s1 += difference[i][0];
		s2 += difference[i][1];
		s3 += difference[i][2];
		s4 += difference[i][3];
	}
	
	avga = 1.*s1/count;
	avgb = 1.*s2/count;
	avgc = 1.*s3/count;
	avgd = 1.*s4/count;
	
	sigma1 = 0;
	sigma2 = 0;
	sigma3 = 0;
	sigma4 = 0;
	
	for (i = 0; i < count; i++) {
		sigma1 += (avga - difference[i][0])*(avga - difference[i][0]);
		sigma2 += (avgb - difference[i][1])*(avgb - difference[i][1]);
		sigma3 += (avgc - difference[i][2])*(avgc - difference[i][2]);
		sigma4 += (avgd - difference[i][3])*(avgd - difference[i][3]);
	}
	
	sigma1 = sqrt(sigma1/count);
	sigma2 = sqrt(sigma2/count);
	sigma3 = sqrt(sigma3/count);
	sigma4 = sqrt(sigma4/count);
	
	printf("A: average = %f, sigma = %f\n", avga, sigma1);
	printf("B: average = %f, sigma = %f\n", avgb, sigma2);
	printf("C: average = %f, sigma = %f\n", avgc, sigma3);
	printf("D: average = %f, sigma = %f\n", avgd, sigma4);
	
	sprintf(str, "%f\n", avga);
	fputs(str, file);
	sprintf(str, "%f\n", sigma1);
	fputs(str, file);
	
	sprintf(str, "%f\n", avgb);
	fputs(str, file);
	sprintf(str, "%f\n", sigma2);
	fputs(str, file);
	
	sprintf(str, "%f\n", avgc);
	fputs(str, file);
	sprintf(str, "%f\n", sigma3);
	fputs(str, file);
	
	sprintf(str, "%f\n", avgd);
	fputs(str, file);
	sprintf(str, "%f\n", sigma4);
	fputs(str, file);
	

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	rewinddir(directory);
	
	t = 0;	
	
	while ( (dir = readdir(directory)) != NULL ) {
		
		if (dir->d_type != 8)
			continue;
			
		strcpy(filename, argv[1]);
		strcat(filename, dir->d_name);
		openbmp(filename, &im);			//	открыли
		
		printf("#%i %s\n", t, dir->d_name);
		
		for (i = 0; i < im.height; i++)
			for (j = 0; j < im.width; j++) {
				im.gr[i][j] = gray(im.r[i][j], im.g[i][j], im.b[i][j]);
				image[i][j] = im.gr[i][j];
			}								//	очернобелили
			
		for (i = 1; i < im.height; i++)
			image[i][0] += image[i-1][0];
		for (j = 1; j < im.width; j++)
			image[0][j] += image[0][j-1];
		for (i = 1; i < im.height; i++)
			for (j = 1; j < im.width; j++)
				image[i][j] = image[i][j] - image[i-1][j-1] + image[i-1][j] + image[i][j-1];		//	представили
				
//---------------------------	A

		maxindex = 0;
		absmax = 0;
		for (height = 20; height < 40; height++)
			for (width = 12; width < 30; width++) {
				maxdiff = 0;
				for (i = im.height/2; i < im.height - height; i++)
					for (j = 1; j < im.width - 2*width; j++) {
						s1 = image[i+height][j+width] + image[i][j] - image[i+height][j] - image[i][j+width];	//	clear
						s2 = image[i+height][j+width + width] + image[i][j + width] - image[i+height][j + width] - image[i][j+width + width];	//	shaded
						diff = s2-s1;
	
						if (diff > maxdiff)
							maxdiff = diff;
					}
					
				if (1.*maxdiff/(height*width) > absmax)
					absmax = 1.*maxdiff/(height*width);
				if (1.*maxdiff/(height*width) > maxindex/* && height > im.height/2 && width > im.width/20*/) {
					maxindex = 1.*maxdiff/(height*width);
					x = height;
					y = width;
				}
			}
		printf(" A: optimal height = %i, width = %i\n", x, y);
		sizea[t][0] = x;
		sizea[t][1] = y;

//---------------------------	B
		maxindex = 0;
		absmax = 0;
		for (height = 5; height < 20; height++)
			for (width = 45; width < 90; width++) {
	
				maxdiff = 0;
				for (i = 1; i < im.height/2; i++)
					for (j = 1; j < im.width - width; j++) {
						s1 = image[i+height][j+width] + image[i][j] - image[i+height][j] - image[i][j+width];		//	shaded
						s2 = image[i+height + height][j+width] + image[i + height][j] - image[i+height + height][j] - image[i + height][j+width];		//	clear
						diff = s1-s2;
	
						if (diff > maxdiff)
							maxdiff = diff;
					}
	
				if (1.*maxdiff/(height*width) > absmax)
					absmax = 1.*maxdiff/(height*width);
				if (1.*maxdiff/(height*width) > maxindex/* && height > im.height/10 && width > im.width/2*/) {
					maxindex = 1.*maxdiff/(height*width);
					x = height;
					y = width;
				}
			}
		printf(" B: optimal height = %i, width = %i\n", x, y);
		sizeb[t][0] = x;
		sizeb[t][1] = y;
//--------------------------------	C

		maxindex = -1000000000;
		absmax = -1000000000;
		for (height = 20; height < 40; height++)
			for (width = 17; width < 45; width++) {
				maxdiff = -100000000;
				for (i = im.height/2; i < im.height - height; i++)
					for (j = 0; j < im.width - 3*width; j++) {
						s1 = image[i+height][j+width] + image[i][j] - image[i+height][j] - image[i][j+width];	//	clear
						s2 = image[i+height][j+width + width] + image[i][j + width] - image[i+height][j + width] - image[i][j+width + width];	//	shaded
						s3 = image[i+height][j+width + width + width] + image[i][j + width + width] - image[i+height][j + width + width] - image[i][j+width + width + width];	//	clear
						diff = s2 - s1 - s3;
						
						diff = -diff;			//	тогда получается положительное число, потому что сумма крайних прямоугольников все равно больше среднего прямоугольника
	
						if (diff > maxdiff)
							maxdiff = diff;
					}
	
				if (1.*maxdiff/(height*width) > absmax)
					absmax = 1.*maxdiff/(height*width);
				if (1.*maxdiff/(height*width) > maxindex/* && height > im.height/2 && width > im.width/10*/) {
					maxindex = 1.*maxdiff/(height*width);
					x = height;
					y = width;
				}
			}
		printf(" С: optimal height = %i, width = %i\n", x, y);
		sizec[t][0] = x;
		sizec[t][1] = y;
//----------------
		t++;
	}

	s1 = 0;
	s2 = 0;
	for (i = 0; i < count; i++) {
		s1 += sizea[i][0];
		s2 += sizea[i][1];
	}
	
	avg = 1.*s1/count;
	aheight = avg + .5;
	avg = 1.*s2/count;
	awidth = avg + .5;
	printf("\nA2: average height = %i, av. width = %i\n", aheight, awidth);
	
	s1 = 0;
	s2 = 0;
	for (i = 0; i < count; i++) {
		s1 += sizeb[i][0];
		s2 += sizeb[i][1];
	}
	
	avg = 1.*s1/count;
	bheight = avg + .5;
	avg = 1.*s2/count;
	bwidth = avg + .5;
	printf("B2: average height = %i, av. width = %i\n", bheight, bwidth);
	
	s1 = 0;
	s2 = 0;
	for (i = 0; i < count; i++) {
		s1 += sizec[i][0];
		s2 += sizec[i][1];
	}
	
	avg = 1.*s1/count;
	cheight = avg + .5;
	avg = 1.*s2/count;
	cwidth = avg + .5;
	printf("C2: average height = %i, av. width = %i\n\n", cheight, cwidth);
	
//===================================================================================================================================================
	
	rewinddir(directory);
	
	t = 0;
	
	while ( (dir = readdir(directory)) != NULL ) {
	
		if (dir->d_type != 8)
			continue;
			
		strcpy(filename, argv[1]);
		strcat(filename, dir->d_name);
		openbmp(filename, &im);			//	открыли
		
		printf("#%i %s\n", t, dir->d_name);
		
		for (i = 0; i < im.height; i++)
			for (j = 0; j < im.width; j++) {
				im.gr[i][j] = gray(im.r[i][j], im.g[i][j], im.b[i][j]);
				image[i][j] = im.gr[i][j];
			}								//	очернобелили
			
		for (i = 1; i < im.height; i++)
			image[i][0] += image[i-1][0];
		for (j = 1; j < im.width; j++)
			image[0][j] += image[0][j-1];
		for (i = 1; i < im.height; i++)
			for (j = 1; j < im.width; j++)
				image[i][j] = image[i][j] - image[i-1][j-1] + image[i-1][j] + image[i][j-1];		//	представили
	
//---------------------------	A	
		x = -1;
		y = -1;
				
		maxdiff = 0;
		for (i = im.height/2; i < im.height - aheight; i++)
			for (j = 0; j < im.width - 2*awidth; j++) {
				s1 = image[i+aheight][j+awidth] + image[i][j] - image[i+aheight][j] - image[i][j+awidth];
				s2 = image[i+aheight][j+awidth + awidth] + image[i][j + awidth] - image[i+aheight][j + awidth] - image[i][j+awidth + awidth];
				
				diff = s2 - s1;
				if (diff > maxdiff) {
					maxdiff = diff;
					x = i;
					y = j;
				}
			}
			
		printf("A: optimal place: x = %i, y = %i\n", x, y);
		coorda[t][0] = x;
		coorda[t][1] = y;

//---------------------------	B

		x = -1;
		y = -1;
		
		maxdiff = 0;
		for (i = 0; i < im.height/2 - bheight; i++)
			for (j = 0; j < im.width - bwidth; j++) {
				s1 = image[i+bheight][j+bwidth] + image[i][j] - image[i+bheight][j] - image[i][j+bwidth];
				s2 = image[i+bheight + bheight][j+bwidth] + image[i + bheight][j] - image[i+bheight + bheight][j] - image[i + bheight][j+bwidth];
				
				diff = s1 - s2;
	
				if (diff > maxdiff) {
					maxdiff = diff;
					x = i;
					y = j;
				}
			}
		printf("B: optimal place: x = %i, y = %i\n", x, y);
		coordb[t][0] = x;
		coordb[t][1] = y;

//---------------------------	C

		x = -1;
		y = -1;
	
		maxdiff = -100000000;
		for (i = 0; i < im.height - cheight; i++)
			for (j = 0; j < im.width - 3*cwidth; j++) {
				s1 = image[i+cheight][j+cwidth] + image[i][j] - image[i+cheight][j] - image[i][j+cwidth];	//	ccwidthear
				s2 = image[i+cheight][j+cwidth + cwidth] + image[i][j + cwidth] - image[i+cheight][j + cwidth] - image[i][j+cwidth + cwidth];	//	shaded
				s3 = image[i+cheight][j+cwidth + cwidth + cwidth] + image[i][j + cwidth + cwidth] - image[i+cheight][j + cwidth + cwidth] - image[i][j+cwidth + cwidth + cwidth];	//	ccwidthear
				diff = s2 - s1 - s3;
//				diff = -diff;
				if (diff > maxdiff) {
					maxdiff = diff;
					x = i;
					y = j;
				}
			}
		printf("C: optimal place: x = %i, y = %i\n", x, y);
		coordc[t][0] = x;
		coordc[t][1] = y;
//---------------
		t++;
	}
	
	s1 = 0;
	s2 = 0;
	for (i = 0; i < count; i++) {
		s1 += coorda[i][0];
		s2 += coorda[i][1];
	}
	
	avg = 1.*s1/count;
	ax = avg + .5;
	avg = 1.*s2/count;
	ay = avg + .5;
	printf("A2: average x = %i, av. y = %i\n", ax, ay);
	
	s1 = 0;
	s2 = 0;
	for (i = 0; i < count; i++) {
		s1 += coordb[i][0];
		s2 += coordb[i][1];
	}
	
	avg = 1.*s1/count;
	bx = avg + .5;
	avg = 1.*s2/count;
	by = avg + .5;
	printf("A2: average x = %i, av. y = %i\n", bx, by);
	
	s1 = 0;
	s2 = 0;
	for (i = 0; i < count; i++) {
		s1 += coordc[i][0];
		s2 += coordc[i][1];
	}
	
	avg = 1.*s1/count;
	cx = avg + .5;
	avg = 1.*s2/count;
	cy = avg + .5;
	printf("A2: average x = %i, av. y = %i\n", cx, cy);

	for (i = ax; i < ax + aheight; i++)
		for (j = ay; j < ay + 2*awidth; j++)
			if (i == ax || i == ax + aheight -1 || j == ay || j == ay + awidth - 1 || j == ay + 2*awidth -1) {
				im.r[i][j] = 255;
				im.g[i][j] = 0;
				im.b[i][j] = 0;
			}
	
	for (i = bx; i < bx + 2*bheight; i++)
		for (j = by; j < by + bwidth; j++)
			if (i == bx || i == bx + bheight -1 || j == by || j == by + bwidth - 1 || i == bx + 2*bheight -1) {
				im.r[i][j] = 0;
				im.g[i][j] = 255;
				im.b[i][j] = 0;
			}
	
	for (i = cx; i < cx + cheight; i++)
		for (j = cy; j < cy + 3*cwidth; j++)
			if (i == cx || i == cx + cheight -1 || j == cy || j == cy + cwidth - 1 || j == cy + 2*cwidth -1 || j == cy + 3*cwidth - 1) {
				im.r[i][j] = 0;
				im.g[i][j] = 0;
				im.b[i][j] = 255;
			}
	

	savebmp("haar2.bmp", im.height, im.width, im.r, im.g, im.b);
	
//==================================================================================================================================

	rewinddir(directory);

	t = 0;
	
	while ( (dir = readdir(directory)) != NULL) {
	
		if (dir->d_type != 8)			//	понятно
			continue;
			
		strcpy(filename, argv[1]);
		strcat(filename, dir->d_name);
		openbmp(filename, &im);			//	открыли
		
		for (i = 0; i < im.height; i++)
			for (j = 0; j < im.width; j++) {
				im.gr[i][j] = gray(im.r[i][j], im.g[i][j], im.b[i][j]);
				image[i][j] = im.gr[i][j];
			}								//	очернобелили
			
		for (i = 1; i < im.height; i++)
			image[i][0] += image[i-1][0];
		for (j = 1; j < im.width; j++)
			image[0][j] += image[0][j-1];
		for (i = 1; i < im.height; i++)
			for (j = 1; j < im.width; j++)
				image[i][j] = image[i][j] - image[i-1][j-1] + image[i-1][j] + image[i][j-1];		//	представили	
			
//----------------------	A
		s1 = image[ax+aheight][ay+awidth] + image[ax][ay] - image[ax+aheight][ay] - image[ax][ay+awidth];
		s2 = image[ax+aheight][ay+awidth + awidth] + image[ax][ay + awidth] - image[ax+aheight][ay + awidth] - image[ax][ay+awidth + awidth];
		diff = s2-s1;
		difference[t][0] = 1.*diff/(aheight*awidth);
//----------------------	B
		s1 = image[bx+bheight][by+bwidth] + image[bx][by] - image[bx+bheight][by] - image[bx][by+bwidth];
		s2 = image[bx+bheight + bheight][by+bwidth] + image[bx + bheight][by] - image[bx+bheight + bheight][by] - image[bx + bheight][by+bwidth];
		diff = s1 - s2;
		difference[t][1] = 1.*diff/(bheight*bwidth);
//----------------------	C
		s1 = image[cx+cheight][cy+cwidth] + image[cx][cy] - image[cx+cheight][cy] - image[cx][cy+cwidth];	//	ccwidthear
		s2 = image[cx+cheight][cy+cwidth + cwidth] + image[cx][cy + cwidth] - image[cx+cheight][cy + cwidth] - image[cx][cy+cwidth + cwidth];	//	shaded
		s3 = image[cx+cheight][cy+cwidth + cwidth + cwidth] + image[cx][cy + cwidth + cwidth] - image[cx+cheight][cy + cwidth + cwidth] - image[cx][cy+cwidth + cwidth + cwidth];	//	ccwidthear
		diff = s2 - s1 - s3;
		difference[t][2] = 1.*diff/(cheight*cwidth);
//---------------------
printf("%s\n", dir->d_name);
printf("A = %f, B = %f, C = %f\n", difference[t][0], difference[t][1], difference[t][2]);
		t++;
	}
//-------------------------------------------------------------------------	
	s1 = 0;
	s2 = 0;
	s3 = 0;
	s4 = 0;
	
	for (i = 0; i < count; i++) {
		s1 += difference[i][0];
		s2 += difference[i][1];
		s3 += difference[i][2];
	}
	
	avga = 1.*s1/count;
	avgb = 1.*s2/count;
	avgc = 1.*s3/count;
	
	sigma1 = 0;
	sigma2 = 0;
	sigma3 = 0;
	
	for (i = 0; i < count; i++) {
		sigma1 += (avga - difference[i][0])*(avga - difference[i][0]);
		sigma2 += (avgb - difference[i][1])*(avgb - difference[i][1]);
		sigma3 += (avgc - difference[i][2])*(avgc - difference[i][2]);
	}
	
	sigma1 = sqrt(sigma1/count);
	sigma2 = sqrt(sigma2/count);
	sigma3 = sqrt(sigma3/count);
	
	printf("A: average = %f, sigma = %f\n", avga, sigma1);
	printf("B: average = %f, sigma = %f\n", avgb, sigma2);
	printf("C: average = %f, sigma = %f\n", avgc, sigma3);


	fputs("second\n", file);

	sprintf(str, "%i\n", aheight);
	fputs(str, file);
	sprintf(str, "%i\n", awidth);
	fputs(str, file);
	sprintf(str, "%i\n", bheight);
	fputs(str, file);
	sprintf(str, "%i\n", bwidth);
	fputs(str, file);
	sprintf(str, "%i\n", cheight);
	fputs(str, file);
	sprintf(str, "%i\n", cwidth);
	fputs(str, file);

	sprintf(str, "%i\n", ax);
	fputs(str, file);
	sprintf(str, "%i\n", ay);
	fputs(str, file);
	sprintf(str, "%i\n", bx);
	fputs(str, file);
	sprintf(str, "%i\n", by);
	fputs(str, file);
	sprintf(str, "%i\n", cx);
	fputs(str, file);
	sprintf(str, "%i\n", cy);
	fputs(str, file);
	
	sprintf(str, "%f\n", avga);
	fputs(str, file);
	sprintf(str, "%f\n", sigma1);
	fputs(str, file);
	sprintf(str, "%f\n", avgb);
	fputs(str, file);
	sprintf(str, "%f\n", sigma2);
	fputs(str, file);
	sprintf(str, "%f\n", avgc);
	fputs(str, file);
	sprintf(str, "%f\n", sigma3);
	fputs(str, file);
	
	
	fclose(file);
	
}
