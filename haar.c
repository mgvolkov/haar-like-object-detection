#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include </home/ermak2/prog/bmp_functions.c>

main (int argc, char* argv[]) {
	
	const int sampheight = 140,					//	магические числа - размер образцов из выборки с 73-мя моими фейсами
			sampwidth = 140;
	
	struct myimage im;
	im.debug = 0;
	
	FILE *file;
	
	char str[25];
	
	long long diff, s1, s2, s3, s4, maxdiff;
	long long **image;
	int i, j, x, y, k, l, count, t;
	int **sizea, **coorda,			//	в эти массивы будем складывать наилучшую высоту и наилучшие координаты для каждой фотки, чтобы потом их усреднить
		**sizeb, **coordb,
		**sizec, **coordc,
		**sized, **coordd;
		
	int height, width;
	
	int aheight, awidth,
		bheight, bwidth,
		cheight, cwidth,
		dheight, dwidth,
		ax, ay,
		bx, by,
		cx, cy,
		dx, dy;
		
	int height2, width2;								//	Да, это не прям чтоб тру. Но я считаю, что так будет лучше. Хотя бы даже для читаемости кода.
	
	int aheight2, awidth2,
		bheight2, bwidth2,
		cheight2, cwidth2,
		dheight2, dwidth2,
		ax2, ay2,
		bx2, by2,
		cx2, cy2,
		dx2, dy2;
	
	float indexa, indexb, indexc, indexd,
		sigma1, sigma2, sigma3, sigma4,
		inda, indb, indc, indd,
		coef;
//-----	
	int a2height, a2width,
		b2height, b2width,
		c2height, c2width,
		a2x, a2y,
		b2x, b2y,
		c2x, c2y;
	
	int a2height2, a2width2,
		b2height2, b2width2,
		c2height2, c2width2,
		a2x2, a2y2,
		b2x2, b2y2,
		c2x2, c2y2;
	
	float index2a, index2b, index2c,
		sigma21, sigma22, sigma23,
		ind2a, ind2b, ind2c;
	
	float s;
			
	if ( (file = fopen("data.txt", "r") ) == NULL ) {				//	! Сверхсильное заклинание! Файл с результатами обучения должен храниться в файле с названием именно "data.txt"
		perror("Opening file `data,txt'");
		exit(EXIT_SUCCESS);
	}
	
	fgets(str, 25, file);
	height = atoi(str);
	fgets(str, 25, file);
	width = atoi(str);
	
	fgets(str, 25, file);
	aheight = atoi(str);
	fgets(str, 25, file);
	awidth = atoi(str);
	
	fgets(str, 25, file);
	bheight = atoi(str);
	fgets(str, 25, file);
	bwidth = atoi(str);
	
	fgets(str, 25, file);
	cheight = atoi(str);
	fgets(str, 25, file);
	cwidth = atoi(str);
	
	fgets(str, 25, file);
	dheight = atoi(str);
	fgets(str, 25, file);
	dwidth = atoi(str);
	
	fgets(str, 25, file);
	ax = atoi(str);
	fgets(str, 25, file);
	ay = atoi(str);
	
	fgets(str, 25, file);
	bx = atoi(str);
	fgets(str, 25, file);
	by = atoi(str);
	
	fgets(str, 25, file);
	cx = atoi(str);
	fgets(str, 25, file);
	cy = atoi(str);
	
	fgets(str, 25, file);
	dx = atoi(str);
	fgets(str, 25, file);
	dy = atoi(str);
	
	fgets(str, 25, file);
	indexa = atof(str);
	fgets(str, 25, file);
	sigma1 = atof(str);
	
	fgets(str, 25, file);
	indexb = atof(str);
	fgets(str, 25, file);
	sigma2 = atof(str);
	
	fgets(str, 25, file);
	indexc = atof(str);
	fgets(str, 25, file);
	sigma3 = atof(str);
	
	fgets(str, 25, file);
	indexd = atof(str);
	fgets(str, 25, file);
	sigma4 = atof(str);
	
	fgets(str, 25, file);
//----	
	fgets(str, 25, file);
	a2height = atoi(str);
	fgets(str, 25, file);
	a2width = atoi(str);
	
	fgets(str, 25, file);
	b2height = atoi(str);
	fgets(str, 25, file);
	b2width = atoi(str);
	
	fgets(str, 25, file);
	c2height = atoi(str);
	fgets(str, 25, file);
	c2width = atoi(str);
	
	fgets(str, 25, file);
	a2x = atoi(str);
	fgets(str, 25, file);
	a2y = atoi(str);
	
	fgets(str, 25, file);
	b2x = atoi(str);
	fgets(str, 25, file);
	b2y = atoi(str);
	
	fgets(str, 25, file);
	c2x = atoi(str);
	fgets(str, 25, file);
	c2y = atoi(str);
	
	fgets(str, 25, file);
	index2a = atof(str);
	fgets(str, 25, file);
	sigma21 = atof(str);
	
	fgets(str, 25, file);
	index2b = atof(str);
	fgets(str, 25, file);
	sigma22 = atof(str);
	
	fgets(str, 25, file);
	index2c = atof(str);
	fgets(str, 25, file);
	sigma23 = atof(str);
	
	fclose(file);
	
	printf("A\n height = %i\n width = %i\n x = %i\n y = %i\n index = %f\n deviation = %f\n\n", aheight, awidth, ax, ay, indexa, sigma1);
	printf("B\n height = %i\n width = %i\n x = %i\n y = %i\n index = %f\n deviation = %f\n\n", bheight, bwidth, bx, by, indexb, sigma2);
	printf("C\n height = %i\n width = %i\n x = %i\n y = %i\n index = %f\n deviation = %f\n\n", cheight, cwidth, cx, cy, indexc, sigma3);
	printf("D\n height = %i\n width = %i\n x = %i\n y = %i\n index = %f\n deviation = %f\n\n", dheight, dwidth, dx, dy, indexd, sigma4);
	
	
	openbmp(argv[1], &im);
	
	im.gr = (unsigned char**) malloc(sizeof(unsigned char*)*im.height);
	image = (long long**) malloc (sizeof(long long*)*im.height);
	for (i = 0; i < im.height; i++) {
		im.gr[i] = (unsigned char*) malloc(sizeof(unsigned char)*im.width);
		image[i] = (long long*) malloc (sizeof(long long)*im.width);
	}

	for (i = 0; i < im.height; i++)
		for (j = 0; j < im.width; j++) {
			im.gr[i][j] = gray(im.r[i][j], im.g[i][j], im.b[i][j]);
			image[i][j] = im.gr[i][j];
		}								//	очернобелили

	for (i = 1; i < im.height; i++)
		image[i][0] += image[i-1][0];
	for (j = 1; j < im.width; j++)
		image[0][j] += image[0][j-1];
	for (i = 1; i < im.height; i++)
		for (j = 1; j < im.width; j++)
			image[i][j] = image[i][j] - image[i-1][j-1] + image[i-1][j] + image[i][j-1];		//	представили


	coef = .1;

	height2 = height*coef;
	width2 = width*coef;
	
	s = 2;

	while (im.height >= height2 && im.width >= width2) {
	
	height2 = height*coef;
	width2 = width*coef;
	
	aheight2 = aheight*coef;
	awidth2 = awidth*coef;
	bheight2 = bheight*coef;
	bwidth2 = bwidth*coef;
	cheight2 = cheight*coef;
	cwidth2 = cwidth*coef;
	dheight2 = dheight*coef;
	dwidth2 = dwidth*coef;
	
	ax2 = ax*coef;
	ay2 = ay*coef;
	bx2 = bx*coef;
	by2 = by*coef;
	cx2 = cx*coef;
	cy2 = cy*coef;
	dx2 = dx*coef;
	dy2 = dy*coef;
//--------	
	a2height2 = a2height*coef;
	a2width2 = a2width*coef;
	b2height2 = b2height*coef;
	b2width2 = b2width*coef;
	c2height2 = c2height*coef;
	c2width2 = c2width*coef;
	
	a2x2 = a2x*coef;
	a2y2 = a2y*coef;
	b2x2 = b2x*coef;
	b2y2 = b2y*coef;
	c2x2 = c2x*coef;
	c2y2 = c2y*coef;
	
	printf("coef = %f\n height = %i, width = %i\n", coef, height2, width2);
	
	coef *= 1.5;
	
	for (i = 0; i < im.height - height2; i += height2/25 > 1 ? height/25 : 1)
		for (j = 0; j < im.width - width2; j += width2/25 > 1 ? width/25 : 1) {

//----------------------	A
			s1 = image[i + ax2+aheight2][j + ay2+awidth2] + image[i + ax2][j + ay2] - image[i + ax2+aheight2][j + ay2] - image[i + ax2][j + ay2+awidth2];
			s2 = image[i + ax2+aheight2][j + ay2+awidth2 + awidth2] + image[i + ax2][j + ay2 + awidth2] - image[i + ax2+aheight2][j + ay2 + awidth2] - image[i + ax2][j + ay2+awidth2 + awidth2];
			diff = s2-s1;
			inda = 1.*diff/(aheight2*awidth2);
//----------------------	B
			s1 = image[i + bx2+bheight2][j + by2+bwidth2] + image[i + bx2][j + by2] - image[i + bx2+bheight2][j + by2] - image[i + bx2][j + by2+bwidth2];
			s2 = image[i + bx2+bheight2 + bheight2][j + by2+bwidth2] + image[i + bx2 + bheight2][j + by2] - image[i + bx2+bheight2 + bheight2][j + by2] - image[i + bx2 + bheight2][j + by2+bwidth2];
			diff = s1 - s2;
			indb = 1.*diff/(bheight2*bwidth2);
//----------------------	C
			s1 = image[i + cx2+cheight2][j + cy2+cwidth2] + image[i + cx2][j + cy2] - image[i + cx2+cheight2][j + cy2] - image[i + cx2][j + cy2+cwidth2];	//	ccwidth2ear
			s2 = image[i + cx2+cheight2][j + cy2+cwidth2 + cwidth2] + image[i + cx2][j + cy2 + cwidth2] - image[i + cx2+cheight2][j + cy2 + cwidth2] - image[i + cx2][j + cy2+cwidth2 + cwidth2];	//	shaded
			s3 = image[i + cx2+cheight2][j + cy2+cwidth2 + cwidth2 + cwidth2] + image[i + cx2][j + cy2 + cwidth2 + cwidth2] - image[i + cx2+cheight2][j + cy2 + cwidth2 + cwidth2] - image[i + cx2][j + cy2+cwidth2 + cwidth2 + cwidth2];	//	ccwidth2ear
			diff = s2 - s1 - s3;
			indc = 1.*diff/(cheight2*cwidth2);
//----------------------	D	
			s1 = image[i + dx2+dheight2][j + dy2+dwidth2] + image[i + dx2][j + dy2] - image[i + dx2+dheight2][j + dy2] - image[i + dx2][j + dy2+dwidth2];	//	cdwidth2ear
			s2 = image[i + dx2+dheight2][j + dy2+dwidth2 + dwidth2] + image[i + dx2][j + dy2 + dwidth2] - image[i + dx2+dheight2][j + dy2 + dwidth2] - image[i + dx2][j + dy2+dwidth2 + dwidth2];	//	shaded
			s3 = image[i + dx2+dheight2 + dheight2][j + dy2+dwidth2] + image[i + dx2 + dheight2][j + dy2] - image[i + dx2+dheight2 + dheight2][j + dy2] - image[i + dx2 + dheight2][j + dy2+dwidth2];
			s4 = image[i + dx2+dheight2 + dheight2][j + dy2+dwidth2 + dwidth2] + image[i + dx2 + dheight2][j + dy2 + dwidth2] - image[i + dx2+dheight2 + dheight2][j + dy2 + dwidth2] - image[i + dx2 + dheight2][j + dy2+dwidth2 + dwidth2];
			diff = s1 + s4 - s2 - s3;
			indd = 1.*diff/(dheight2*dwidth2);
			
//----------------------- 2
//----------------------	A
			s1 = image[i + a2x2+a2height2][j + a2y2+awidth2] + image[i + a2x2][j + a2y2] - image[i + a2x2+a2height2][j + a2y2] - image[i + a2x2][j + a2y2+awidth2];
			s2 = image[i + a2x2+a2height2][j + a2y2+awidth2 + awidth2] + image[i + a2x2][j + a2y2 + awidth2] - image[i + a2x2+a2height2][j + a2y2 + awidth2] - image[i + a2x2][j + a2y2+awidth2 + awidth2];
			diff = s2-s1;
			ind2a = 1.*diff/(a2height2*awidth2);
//----------------------	B
			s1 = image[i + b2x2+b2height2][j + b2y2+bwidth2] + image[i + b2x2][j + b2y2] - image[i + b2x2+b2height2][j + b2y2] - image[i + b2x2][j + b2y2+bwidth2];
			s2 = image[i + b2x2+b2height2 + b2height2][j + b2y2+bwidth2] + image[i + b2x2 + b2height2][j + b2y2] - image[i + b2x2+b2height2 + b2height2][j + b2y2] - image[i + b2x2 + b2height2][j + b2y2+bwidth2];
			diff = s1 - s2;
			ind2b = 1.*diff/(b2height2*bwidth2);
//----------------------	C
			s1 = image[i + c2x2+c2height2][j + c2y2+c2width2] + image[i + c2x2][j + c2y2] - image[i + c2x2+c2height2][j + c2y2] - image[i + c2x2][j + c2y2+c2width2];	//	cc2width2ear
			s2 = image[i + c2x2+c2height2][j + c2y2+c2width2 + c2width2] + image[i + c2x2][j + c2y2 + c2width2] - image[i + c2x2+c2height2][j + c2y2 + c2width2] - image[i + c2x2][j + c2y2+c2width2 + c2width2];	//	shaded
			s3 = image[i + c2x2+c2height2][j + c2y2+c2width2 + c2width2 + c2width2] + image[i + c2x2][j + c2y2 + c2width2 + c2width2] - image[i + c2x2+c2height2][j + c2y2 + c2width2 + c2width2] - image[i + c2x2][j + c2y2+c2width2 + c2width2 + c2width2];	//	cc2width2ear
			diff = s2 - s1 - s3;
			ind2c = 1.*diff/(c2height2*c2width2);
			
			
			if (inda >= indexa - s*sigma1 && inda <= indexa + s*sigma1
				&& indb >= indexb - s*sigma2 && indb <= indexb + s*sigma2
				&& indc >= indexc - s*sigma3 && indc <= indexc + s*sigma3
				&& indd >= indexd - s*sigma4 && indd <= indexd + s*sigma4) 
				
				if (ind2a >= index2a - s*sigma21 && ind2a <= index2a + s*sigma21
					&& ind2b >= index2b - s*sigma22 && ind2b <= index2b + s*sigma22
					&& ind2c >= index2c - s*sigma23 && ind2c <= index2c + s*sigma23) {
				
				printf("here! i = %i, j = %i\n", i, j);

				for (k = 0; k < height2; k++) {
						im.r[i+k][j] = 255;
						im.g[i+k][j] = 0;
						im.b[i+k][j] = 0;
						im.r[i+k][j+width2] = 255;
						im.g[i+k][j+width2] = 0;
						im.b[i+k][j+width2] = 0;
					}
				for (k = 0; k < height2; k++) {
					im.r[i][j+k] = 255;
					im.g[i][j+k] = 0;
					im.b[i][j+k] = 0;
					im.r[i+height2][j+k] = 255;
					im.g[i+height2][j+k] = 0;
					im.b[i+height2][j+k] = 0;
				}
				
			}
			
		}
		
	}
		
		savebmp("image_r.bmp", im.height, im.width, im.r, im.g, im.b);
		
		
		
		
}
