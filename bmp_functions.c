#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

#define headersize 60

struct myimage {
	char *namename;
	unsigned char header[headersize],
	*buf;
	long height,
	width,
	depth;
	unsigned char **r,
	**g,
	**b,
	**gr;
	char debug;
};
//------------------------------------------------------------------------------
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
//==============================================================================

unsigned char gray (unsigned char r, unsigned char g, unsigned char b) {
	return (unsigned char) (.21*r + .72*g + .07*b + .5);
};

//==============================================================================
void getinfbmp(char *filename, struct myimage *im) {
	
	FILE *file;
	char *message, findpoint = 0;
	long i;
	
	message = (char*) malloc (sizeof(char)*280);
	
if (im->debug)
printf("getinf: opening %s\n", filename);
	
	if ((file = fopen(filename, "rb")) == NULL) {
		sprintf(message, "getinf: Opening file %s", filename);
		perror(message);
		exit(EXIT_SUCCESS);
	}
	
	if (fgets(im->header, headersize, file) == NULL)
		perror("getinfbmp:");
	
	im->width = 256*256*256*im->header[21] + 256*256*im->header[20] + 256*im->header[19] + im->header[18];
	im->height = 256*256*256*im->header[25] + 256*256*im->header[24] + 256*im->header[23] + im->header[22];
	
	im->namename = (char*) malloc (sizeof(char)*strlen(filename));
	
	for (i = strlen(filename)-1; i >= 0; i--) {
		if (findpoint)
			im->namename[i] = filename[i];
		if (filename[i] == '.')
			findpoint++;
		
	}
	
	fclose(file);
}

//==============================================================================
void openbmp (char *filename, struct myimage *im) {
	FILE *file;
	int shift;
	long nwidth;
	char addwidth, findpoint = 0;
	long i, j, bufj;
	char *message;
if (im->debug)
perror("Opnenbmp: Start");
	if((file = fopen(filename, "rb")) == NULL) {
		sprintf(message, "Opening file %s", filename);
		perror(message);
		exit(EXIT_SUCCESS);
	}
if (im->debug)
perror("openbmp: file is opened");
	im->namename = (char*) malloc (sizeof(char)*strlen(filename));

	
	if (fgets(im->header, headersize, file) == NULL)
		perror("openbmp:");

	im->width = 256*256*256*im->header[21] + 256*256*im->header[20] + 256*im->header[19] + im->header[18];
	nwidth = 3*im->width -1 + 4 - (3*im->width - 1) % 4;
	addwidth = nwidth - 3*im->width;
	im->height = 256*256*256*im->header[25] + 256*256*im->header[24] + 256*im->header[23] + im->header[22];
	shift = im->header[10];

	fseek(file, shift, SEEK_SET);
if (im->debug)
perror("openbmp: data");
	im->buf = (unsigned char*) malloc(sizeof(unsigned char)*nwidth);

	im->r = (unsigned char**) malloc(sizeof(unsigned char*)*im->height);
	for (i = 0; i < im->height; i++)
		im->r[i] = (unsigned char*) malloc(sizeof(unsigned char)*im->width);

	im->g = (unsigned char**) malloc(sizeof(unsigned char*)*im->height);
	for (i = 0; i < im->height; i++)
		im->g[i] = (unsigned char*) malloc(sizeof(unsigned char)*im->width);

	im->b = (unsigned char**) malloc(sizeof(unsigned char*)*im->height);
	for (i = 0; i < im->height; i++)
		im->b[i] = (unsigned char*) malloc(sizeof(unsigned char)*im->width);
if (im->debug)
perror("openbmp: memory");
//------------- Работает:
	for (i = 0; i < im->height; i++) {
		for (j = 0; j < im->width; j++) {
			im->b[i][j] = fgetc(file);
			im->g[i][j] = fgetc(file);
			im->r[i][j] = fgetc(file);
		}
		fseek(file, addwidth, SEEK_CUR);
	}
if (im->debug)
perror("openbmp: read");
	for (i = strlen(filename)-1; i >= 0; i--) {
		if (findpoint)
			im->namename[i] = filename[i];
		if (filename[i] == '.')
			findpoint++;
		
	}

if (im->debug)
perror("openbmp: end");
//printf("height = %li, width = %li\n", im->height, im->width);

	fclose(file);
	
}

//============================================================================================
int little_endian (long num, int radix, unsigned short k) {
	long num2;
	int *number, i, counter;

	num2 = num;
	counter = 0;
	while (num2) {
		counter++;
		num2 /= radix;
	}

	number = (int*) malloc(sizeof(int)*counter);

	for (i = counter-1; i > 0; i--) {
		number[i] = num / pow(radix, i);
		num2 = pow(radix, i);
		num %= num2;
	}
	
	number[0] = num % radix;

	if (!k)
		return(counter);
	else
		return(number[k-1]);
}

//===============================================================================================================
void savebmp (char *filename, long height, long width, unsigned char **rnew, unsigned char **gnew, unsigned char **bnew) {
	struct myimage im;
	long imagesize, filesize;
	FILE *out;
	unsigned char header[54] = {66, 77, 0,	/*filesize 4	**/ 0, 0, 0, 0, 0, 0, 0, 54,	/*shift 4 */ 0, 0, 0, 40, 0, 0, 0, 0,	/*width 4		**/ 0, 0, 0, 0,	/*height 4	**/ 0, 0, 0, 1, 0, 24, 0, 0, 0, 0, 0, 0,	/*imagesize = filesize - 54	4 */ 0, 0, 0, 19, 11, 0, 0, 19, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	char shift, addwidth;
	unsigned char *buf;
	long nwidth;
	long i, j;

	for (i = 18; i < 18+little_endian(width, 256, 0); i++) {
		header[i] = little_endian(width, 256, i-17);
	}
	for (i = 22; i < 22+little_endian(height, 256, 0); i++) {
		header[i] = little_endian(height, 256, i-21);
	}

	nwidth = 3*width -1 + 4 - (3*width - 1) % 4;
	addwidth = nwidth - 3*width;
	shift = header[10];

	imagesize = height*width*3 + addwidth*height;
	filesize = imagesize + 54;

	for (i = 34; i < 34+little_endian(imagesize, 256, 0); i++) {
		header[i] = little_endian(imagesize, 256, i-33);
	}
	for (i = 2; i < 2+little_endian(filesize, 256, 0); i++) {
		header[i] = little_endian(filesize, 256, i-1);
	}

	buf = (unsigned char*) malloc(sizeof(unsigned char)*nwidth);

	if ( (out = fopen(filename, "wb")) == NULL)
		perror("Saving file");

	for (i = 0; i < shift; i++)
		fputc(header[i], out);

	for (i = 0; i < height; i++) {
		for (j = 0; j < nwidth; j++) {
			if (j < nwidth-addwidth) {
				switch (j%3)
				{
					case 0: buf[j] = bnew[i][j/3]; break;
					case 1: buf[j] = gnew[i][j/3]; break;
					case 2: buf[j] = rnew[i][j/3]; break;
				}
			}
			else {
				buf[j] = 0;
			}
		}
		fwrite(buf, 1, nwidth, out);
	}

	fclose(out);
}

//======================================================================================================

int compare(int num1, int num2, int interval) {
	if (num1 >= num2 - interval && num1 <= num2 + interval)
		return 1;
	else
		return 0;
}
//------------------------------------------------------------------
int in_interval(int num, int floor, int ceiling) {
	if (num > floor && num < ceiling)
		return (1);
	else
		return (0);
}

//=======================================================================================================
void resize(unsigned char **in, long height, long width, float coef, struct myimage *im) {

	unsigned char **newimg;
	long i, j, k, l;
	
	im->height = height*coef;
	im->width = width*coef;
	
	im->gr = (unsigned char**) malloc (sizeof(unsigned char*)*im->height);
	for (i = 0; i < im->height; i++)
		im->gr[i] = (unsigned char*) malloc (sizeof(unsigned char)*im->width);
	
	for (i = 0; i < im->height; i++)
		for (j = 0; j < im->width; j++) {
			k = i/coef;
			l = j/coef;
			im->gr[i][j] = in[k][l];
		}
		
//	savebmp("resized.bmp", im->height, im->width, im->gr, im->gr, im->gr);
}
//=======================================================================================================

void resize2(unsigned char **in, long height, long width, long newheight, long newwidth, struct myimage *im) {

	long i, j, k, l;
	float hcoef, wcoef;

	hcoef = newheight/height;
	wcoef = newwidth/width;

if (im->debug)
printf("old size: %lix%li - new size: %lix%li\n", height, width, newheight, newwidth);
	
	im->gr = (unsigned char**) malloc (sizeof(unsigned char*)*newheight);
	for (i = 0; i < newheight; i++)
		im->gr[i] = (unsigned char*) malloc (sizeof(unsigned char)*newwidth);
	
	for (i = 0; i < newheight; i++)
		for (j = 0; j < newwidth; j++) {
			k = 1.*i/newheight*height;
			l = 1.*j/newwidth*width;
if (i > newheight - 2 && j > newwidth - 2)
printf("i = %li, j = %li - k = %li, l = %li\n", i, j, k, l);
			im->gr[i][j] = in[k][l];
		}
		
//	savebmp("resized.bmp", im->height, im->width, im->gr, im->gr, im->gr);
}

//=======================================================================================================

void histogram(unsigned char **image, long height, long width, char ch, struct myimage *im) {
	long i, j, imax;
	long long sum[256], m = 0, m2 = 0;
	unsigned char **histogramma, *outname;
	int h = 256,
	w = 256;

	for (i = 0; i < 256; i++)
		sum[i] = 0;

	for (i = 0; i < height; i++)
		for (j = 0; j < width; j++) {
			sum[image[i][j]]++;
		}

	for (i = 0; i < 256; i++)
		if (sum[i] > m) {
			m2 = m;
			m = sum[i];
		}
	
printf("max = %lli\n", m);
	histogramma = (unsigned char**) malloc(sizeof(unsigned char*)*h);
	for (i = 0; i < h; i++)
		histogramma[i] = (unsigned char*) malloc(sizeof(unsigned char)*w);
		
	for (i = 0; i < h; i++)
		for (j = 0; j < w; j++) {
			if (i < sum[j]*255/m) {
				histogramma[i][j] = 255;
			}
			else
				histogramma[i][j] = 0;
		}
		
	outname = (char*) malloc(sizeof(char)*256);
	if (ch == 'r')
		sprintf(outname, "%s_histogram_red.bmp", im->namename);
	else
	if (ch == 'g')
		sprintf(outname, "%s_histogram_green.bmp", im->namename);
	else
		sprintf(outname, "%s_histogram_blue.bmp", im->namename);
		
	savebmp(outname, h, w, histogramma, histogramma, histogramma);
}
